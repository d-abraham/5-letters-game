Game Description:

The game is set of a 5x5 grid where each square on the grid contains a single
letter, which is randomly assigned one of 5 possible letters (a, b, c, d, and e).
The objective is to collect one of each letter and letters are collected by 
clicking on a square to obtain the letter inside. Each player can click on a 
square adjacent to the one they are currently on, per turn. Each player will 
have an inventory of the letters they have collected, if a latter exist in the 
inventory or not, it will be added and override the older letter. The first 
player to have an inventory of all 5 unique letters will trigger the win 
condition.