import java.awt.*;
import javax.swing.JButton;

/**
 * @author d-abraham
 * @author Louis Hwang
 */
public class Button extends JButton {

    private Point point;
    char value;
    /**
     * Construct Button with the default point (0,0) and char values
     */
    public Button(){
        point = new Point();
        value = ' ';
    }
    /**
     * Construct button with specified x and y coordinates and default char and specified background values
     * @param x coordinate for the Button
     * @param y coordinate for the Button
     *
     */
    public Button(int x,int y){
        point = new Point(x,y);
        value = ' ';
        setBackground(Color.BLACK);
        setForeground(Color.GREEN);
    }

    /**
     * Set new x and y int values for Point of Button
     * @param x coordinate of the point
     * @param y coordinate of the point
     *
     */
    public void setPoint(int x,int y){
        point.setLocation(x,y);
        value = ' ';
        setBackground(Color.DARK_GRAY);
        setForeground(Color.GREEN);
    }

    /**
     * Return Point
     * @return Point
     */
    public Point getPoint(){
        return point;
    }

    /**
     * Return x value of point
     * @return x value of point
     */
    public int getXPoint(){
        return (int)point.getX();
    }

    /**
     * Return y value of point
     * @return y value of point
     */
    public int getYPoint(){
        return (int) point.getY();
    }

    /**
     * Return char value
     * @return char value
     */
    public char getValue(){return value;}

    /**
     * Set new value
     * @param value
     *
     */
    public void setValue(char value){
        this.value = value;
    }
     public String toString(){
        return (int)point.getX()+"-"+(int)point.getY() + " value "+value + " text "+ getText();
     }
}
