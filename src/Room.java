
import java.util.LinkedList;
import java.util.ListIterator;
/**
 * Room object represents a room in a game-server which hold multiple players using
 * LinkedList.
 * @author Louis Hwang
 * @author d-abraham
 *
 */
public class Room {
    private LinkedList<Player> playerList;
    private String id;

    /**
     * Construct a Room with default LinkedList and String id
     */
    public Room(){
        playerList = null;
        id = "0";
    }

    /**
     * Construct a Room with initialized LinkedList and specified String id
     * @param id
     */
    public Room(String id){
        playerList = new LinkedList<Player>();
        this.id = id;
    }

    /**
     *  returns Linked List of Room
     * @return playerList
     */
    public LinkedList<Player> getPlayerList(){
        return playerList;
    }

    /**
     *  returns String id of Room
     * @return Room-id
     */
    public String getId(){
        return id;
    }

    /**
     *  add specified Player to playerList
     * @param player player to add to playerList
     */
    public void addPlayer(Player player){
        playerList.add(player);
    }

    /**
     *  remove specified Player from playerList
     * @param player player to remove from playerList
     */
    public void removePlayer(Player player){
        playerList.remove(player);
    }

    /**
     *  remove specified Player using String from playerList
     * @param  playName of player to remove from playerList
     */
    public void removePlayer(String playName){
        ListIterator<Player> list = playerList.listIterator(0);
        while (list.hasNext()){
            if(list.next().getId().matches(playName)){
                list.remove();
            }
        }
    }

    /**
     *  Return a random player within the same room as the given player.
     * @param player
     */
    public Player getRandomPlayer(Player player){
        ListIterator<Player> list = playerList.listIterator(0);
        while (list.hasNext()){
            Player tmp=list.next();
            if(!tmp.equals(player)){
                return tmp;
            }
        }
        return null;
    }

    /**
     * Returns a string representation of Room
     * @return a string representation of Room data fields
     */
    public String toString(){
        ListIterator<Player> list= playerList.listIterator(0);
        String tmp="Room ID: "+id+"\n"+"Player(s) in the room:\n";
        while (list.hasNext()){
            tmp+=list.next().toString()+"\n";
        }
        return tmp;
    }

    /**
     * Returns boolean determining if specified object is the same
     * @return boolean
     */
    public boolean equals(Object obj){
        if (obj == this) return true;

        if (obj == null) return false;

        if (this.getClass() == obj.getClass()){
            Room a = (Room) obj;

            return this.getPlayerList().equals(a.getPlayerList()) &&
                    this.getId().equals(a.getId());
        }
        else
            return false;
    }
}