
import java.util.ArrayList;


/**
 * Server object represents a server-game that hold multiple Rooms objects. Players
 * location is determined by the hash(). Also player can change room by changing the
 * player kay and relocate the according to the new key=>hash()
 * @author Louis Hwang
 * @author d-abraham
 */
public class Server {

    private ArrayList<Room> roomArray;
    private int numberOfRooms;

    /**
     * Construct a Server with default ArrayList roomArray and int numberOfRooms
     */
    public Server(){
        roomArray = null;
        numberOfRooms = 0;
    }

    /**
     * Construct a Room with specified int numberOfRooms
     */
    public Server(int numberOfRooms){
        roomArray = new ArrayList<Room>(numberOfRooms);
        this.numberOfRooms = numberOfRooms;
        for (int x=0; x<numberOfRooms; x++){
            roomArray.add(new Room("Room-"+x));
            getRoom(x).addPlayer(new Player("player-"+(char)(int)
                    (65+(Math.random()*26)),""+x));
        }
    }

    /**
     *  returns specified Room at index of roomArray
     * @param a index of roomArray
     */
    public Room getRoom(int a){
        return roomArray.get(a);
    }

    /**
     *  Adds player to Room
     * @param player Player being added
     */
    public void addPlayer(Player player){
        getRoom(player.hash()%numberOfRooms).addPlayer(player);
    }

    /**
     * Returns the player room
     * @param player Player being searched for
     * @return player's current room
     */
    public Room getPlayerRoom(Player player){
        return roomArray.get(player.hash()%roomArray.size());
    }

    /**
     *  change Room of specified player
     * @param player Player changing rooms
     */
    public void changeRoom(Player player){
        Player tmp = player;
        getRoom(player.hash()%numberOfRooms).removePlayer(player);
        changePlayerKey(tmp);
        getRoom(tmp.hash()%numberOfRooms).addPlayer(tmp);
    }

    /**
     *  change Key of specified player
     * @param player Player whose key is being changed
     */
    private void changePlayerKey(Player player){
        String newKey = ""+((int)(Math.random()*999));
        if(player.getKey().matches(newKey)){
            newKey += "1";
        }
        player.setKey(newKey);
    }

    /**
     *  returns hash using specified key
     * @param key String that is used to calculate hash
     */
    public int hash(String key) {
        char[] alpha = {'a','b','c','d','e','f','g','h','i','j','k','l',
                'm','n','o','p','q','r','s','t','u','v','w','x','y','z','0','1','2',
                '3','4','5','6','7','8','9'};
        int hash=0;

        for(int i =0; i<key.length(); i++) {
            for(int c=0; c<alpha.length; c++) {
                if(key.charAt(i) == alpha[c]) {
                    hash = hash+((i+1)*(c+1));
                    break;
                }
            }
        }
        return hash;
    }

    /**
     * Returns a string representation of Server
     * @return a string representation of rooms within Server
     */
    public String toString(){
        String str = "Number(s) of room(s):"+numberOfRooms+"\n";
        for(int i=0; i<numberOfRooms;i++){
            str += getRoom(i).toString();
        }
        return str;
    }

    /**
     * Returns boolean determining if specified object is the same
     * @return boolean
     */
    public boolean equals(Object obj){
        if (obj == this) return true;

        if (obj == null) return false;

        if (this.getClass() == obj.getClass()){
            Server a = (Server) obj;

            return true;
        }
        else
            return false;
    }

}