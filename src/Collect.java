import javax.swing.JPanel;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.Arrays;
/**
 * Collect letters by moving your player piece through the squares. Move your piece by
 * clicking squares adjacent to your current square(up,down,left and right).
 * The first to collect one of each of the 5 letters wins the game
 * @author Louis Hwang
 * @author d-abraham
 */
public class Collect extends JPanel{

    private Button[][] buttons;

    /**
     * Construct Collect with 2D Array of Buttons in 5x5 GridLayout.
     */    public Collect(){
        //info = "";
        buttons = new Button[5][5];
        setSize(new Dimension(400,400));
        setLayout(new GridLayout(5,5));
        //Initialize LetterSquares
        for(int row = 0; row < 5; row++){
            for(int column = 0; column < 5; column++){
                buttons[row][column] = new Button(row,column);
                buttons[row][column].setSize(5,5);
                add(buttons[row][column]);
            }
        }
    }

    /**
     * Construct button with specified x and y coordinates and default char and specified background values
     * @param info String for Collect
     * @param buttonArray 2D Array that holds Buttons
     */
    public Collect(String info, Button[][] buttonArray){
        this.buttons = buttonArray;

    }

    /**
     * Remove letter at specific row and column in 2D Array of buttons
     * @param row row of the 2D Array
     * @param column column of the 2D Array
     *
     */
    public void removeLetter(int row, int column){
        buttons[row][column].setText(" ");
        buttons[row][column].setValue(' ');
    }

    /**
     * Determines the winner
     * @param player player that is checked for winning
     *
     */
    public boolean isWinner(Player player){
        char[] letterComplete = {'A', 'B', 'C', 'D', 'E'};
        char[] tmp = player.getCharArray().clone();
        Arrays.parallelSort(tmp);
        return Arrays.equals(tmp, letterComplete);
    }

    /**
     * Return Button[][]
     * @return Button[][]
     */
    public Button[][] getButtons(){
        return buttons;
    }

    /**
     * Return char from specified row and column
     * @return char
     */
    public char getLetter(int row, int column){
        return buttons[row][column].getValue();
    }

    /**
     * Return Button from specified row and column
     * @return Button
     */
    public Button getButton(int row, int column){
        return buttons[row][column];
    }

    /**
     * Randomly assigns letters to 2D Array buttons
     *
     */
    public void randomizeLetters(){
        char[] tmp={'A','B','C','D','E'};
        int index =0;
        for (int row=0; row<5;row++){
            for (int column=0; column<5;column++){
                index = (int)((Math.random())*5);
                buttons[row][column].setText(""+tmp[index]);
                buttons[row][column].setValue(tmp[index]);
            }
        }
    }

    /**
     * Returns boolean determining if specified object is the same
     * @return boolean
     */
    public boolean equals(Object obj){
        if (obj == this) return true;

        if (obj == null) return false;

        if (this.getClass() == obj.getClass()){
            Collect a = (Collect) obj;

            return true;

        }
        else
            return false;
    }

    /**
     * Returns a string representation of buttons of Collect
     * @return A string representation of buttons of Collect
     */
    public String toString(){
        String str = " ";
        for (int row=0; row<5;row++){
            for (int column=0; column<5;column++){
                str += buttons[row][column].toString()+" -- ";
            }
            str+="\n";
        }
        return str;
    }
}
