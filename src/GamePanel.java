import java.awt.Color;
import java.awt.Font;
import java.awt.Dimension;
import java.awt.BorderLayout;
import java.awt.Point;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JOptionPane;
import javax.swing.JFrame;

/**
 *
 * @author Louis Hwang
 *
 * @author d-abraham
 */
public class GamePanel extends JPanel implements ActionListener{

    private JButton startButton, changeRoom;
    private JPanel buttons;
    private JScrollPane scrollPane;
    private JTextArea instructions, serverInfo;
    private Collect collect;
    private Server server;
    Player player1;
    Player player2;
    //Determines which player's turn it is
    private boolean player1Turn = true;
    private boolean player2Turn = false;

    /**
     * Construct GamePanel, initializing GUI and collect, player, button, and server objects
     */    public GamePanel(){
        collect = new Collect();
        server = new Server(10);
        player1 = new Player(JOptionPane.showInputDialog(this,"Enter your name:"));
        server.addPlayer(player1);
        player2 = server.getRoom(player1.hash()%10).getRandomPlayer(player1);
        startButton = new JButton("Start");
        changeRoom = new JButton("Change Room");
        buttons = new JPanel();
        buttons.setBackground(Color.BLACK);
        serverInfo = new JTextArea(25,10);
        serverInfo.setFont(new Font("",Font.PLAIN,16));
        scrollPane = new JScrollPane(serverInfo);
        scrollPane.setPreferredSize(new Dimension(220,15));
        startButton.addActionListener(this);
        changeRoom.addActionListener(this);
        buttons.add(startButton);
        buttons.add(changeRoom);
        setLayout(new BorderLayout());
        this.add(buttons, BorderLayout.PAGE_START);
        this.add(collect,BorderLayout.CENTER);
        serverInfo.setEnabled(false);
        serverInfo.setBackground(Color.black);
        serverInfo.setDisabledTextColor(Color.GREEN);
        serverInfo.append(server.toString());
        this.add(scrollPane,BorderLayout.WEST);
        instructions = new JTextArea(5, 15);
        instructions.setFont(new Font("",Font.PLAIN,16));
        instructions.setBackground(Color.black);
        instructions.setForeground(Color.GREEN);
        instructions.setEditable(false);
        this.add(instructions, BorderLayout.PAGE_END);
        for (int x=0; x<5; x++){
            for (int y=0; y<5; y++){
                collect.getButton(x,y).addActionListener(this);
            }
        }
        JOptionPane.showMessageDialog(this,"    INSTRUCTIONS: \n"
                + "    Collect letters by moving your player piece through the squares."
                + " Move your piece by clicking squares adjacent to your \n    current square. The first to collect one of each "
                + " of the 5 letters wins the game \n"+
                "    To re-start the game click on Start button. To change room click"+
                        " on Change Room button to change room.\nNote Player2 will be"+
                " selected from the new room.");
        startNewGame();
        instructions.setText(server.getPlayerRoom(player1).getId()+"\n"+
                player1.getId()+": "+player1.showList()+"\n"+
                player2.getId()+": " + player2.showList());
    }

    /**
     * Actions performed
     */
    public void actionPerformed(ActionEvent e) {

        if (e.getActionCommand().matches("Start")) {
            startNewGame();
            instructions.setText(server.getPlayerRoom(player1).getId()+"\n"+
                    player1.getId()+": "+player1.showList()+"\n"+
                    player2.getId()+": " + player2.showList());
        } else if (e.getActionCommand().matches("Change Room")) {
            server.changeRoom(player1);
            player2 = server.getRoom(player1.hash() % 10).getRandomPlayer(player1);
            serverInfo.setText(server.toString());
            startNewGame();
            instructions.setText(server.getPlayerRoom(player1).getId()+"\n"+
                    player1.getId()+": "+player1.showList()+"\n"+
                    player2.getId()+": " + player2.showList());
        } else {
            Button clickedB = (Button) e.getSource();
            System.out.println(clickedB.toString());
            if (player1Turn) {
                if (player1.getPoint().getX() == -1) { //First turn
                    player1.setPoint(clickedB.getPoint());
                    player1.addToCharArray(clickedB.getValue());
                    clickedB.setText(player1.getId());
                    clickedB.setValue(' ');
                    flipTurns();
                    instructions.setText(server.getPlayerRoom(player1).getId()+"\n"+
                            player1.getId()+": "+player1.showList()+"\n"+
                            player2.getId()+": " + player2.showList());
                } else { // 2nd  turn or later
                    if (isAllowedMove(player1.getPoint(), clickedB.getPoint())) {
                        collect.getButton((int) player1.getPoint().getX(),
                                (int) player1.getPoint().getY()).setText(" ");
                        collect.getButton((int) player1.getPoint().getX(),
                                (int) player1.getPoint().getY()).setValue(' ');
                        System.out.println(player1.toString());
                        player1.setPoint(clickedB.getPoint());
                        player1.addToCharArray(clickedB.getValue());
                        clickedB.setText(player1.getId());
                        clickedB.setValue(' ');
                        flipTurns();
                        instructions.setText(server.getPlayerRoom(player1).getId()+"\n"+
                                player1.getId()+": "+player1.showList()+"\n"+
                                player2.getId()+": " + player2.showList());
                        if (collect.isWinner(player1)) {// check if game is over
                            JOptionPane.showMessageDialog(this,
                                    player1.getId()+" wins");
                        }
                    } else {
                        JOptionPane.showMessageDialog(this,
                                player1.getId()+": you can not click here.\n " +
                                        "Only up, down, left and right.");
                    }
                }
            } else if (player2Turn) {//player 2
                    if (player2.getPoint().getX() == -1) { //First turn
                        player2.setPoint(clickedB.getPoint());
                        player2.addToCharArray(clickedB.getValue());
                        clickedB.setText(player2.getId());
                        clickedB.setValue(' ');
                        flipTurns();
                        instructions.setText(server.getPlayerRoom(player1).getId()+"\n"+
                                player1.getId()+": "+player1.showList()+"\n"+
                                player2.getId()+": " + player2.showList());
                    } else { // 2nd  turn or later
                        if (isAllowedMove(player2.getPoint(), clickedB.getPoint())) {
                            collect.getButton((int) player2.getPoint().getX(),
                                    (int) player2.getPoint().getY()).setText(" ");
                            collect.getButton((int) player2.getPoint().getX(),
                                    (int) player2.getPoint().getY()).setValue(' ');
                            System.out.println(player2.toString());
                            player2.setPoint(clickedB.getPoint());
                            player2.addToCharArray(clickedB.getValue());
                            clickedB.setText(player2.getId());
                            clickedB.setValue(' ');
                            flipTurns();
                            instructions.setText(server.getPlayerRoom(player1).getId()+"\n"+
                                    player1.getId()+": "+player1.showList()+"\n"+
                                    player2.getId()+": " + player2.showList());
                            if (collect.isWinner(player2)) {// check if game is over
                                JOptionPane.showMessageDialog(this,
                                        player2.getId()+" wins");
                            }
                        } else {
                            JOptionPane.showMessageDialog(this,
                                    player2.getId()+": you can not click here.\n " +
                                            "Only up, down, left and right.");
                        }
                    }
                }
            }
        }

    /**
     * Swaps boolean values of players' turn
     */
    public void flipTurns(){
        if(player1Turn == true){
            player1Turn = false;
            player2Turn = true;
        }
        else {
            player1Turn = true;
            player2Turn = false;
        }
    }

    /**
     * Determines if player can move to specified Point
     */
    public boolean isAllowedMove(Point playerPointCurrent, Point playerNextPoint){
        if(playerPointCurrent.getX() == playerNextPoint.getX()-1 &&
        playerPointCurrent.getY() == playerNextPoint.getY()){
            return true;
        }
        else if(playerPointCurrent.getX() == playerNextPoint.getX() &&(
                playerPointCurrent.getY() == playerNextPoint.getY()-1 ||
                        playerPointCurrent.getY() == playerNextPoint.getY()+1)){
            return true;
        }
        else if(playerPointCurrent.getX() == playerNextPoint.getX()+1 &&
                playerPointCurrent.getY() == playerNextPoint.getY()){
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Start or restart the game and rests the players.
     */
    public void startNewGame(){
        collect.randomizeLetters();
        player1.restList();
        player1.restPoint();
        player2.restList();
        player2.restPoint();
        player1Turn = true;
        player2Turn = false;
    }

    public static void main(String[] args) {
        // main method
        GamePanel t = new GamePanel();
        JFrame frame = new JFrame("Game");
        frame.setBackground(Color.DARK_GRAY);
        frame.setForeground(Color.DARK_GRAY);
        Container c = frame.getContentPane();
        c.add(t);
        frame.setPreferredSize(new Dimension(1100, 900));
        frame.setBackground(Color.WHITE);
        frame.pack();
        frame.setVisible(true);
        // the program will exit when the frame is closed
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

}