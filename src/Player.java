import java.awt.Point;
import java.util.Arrays;

/**
 *
 * @author Louis Hwang
 * @author d-abraham
 *
 * Player object represents a participating player in a game
 */

public class Player {
    private String id;
    private Point point;
    private char[] charArray;
    private String key;
    private int index;

    /**
     * Construct Player with ID, default Char Array, Key int, Point, and Index int
     */
    public Player(){
        id = "No ID";
        charArray = new char[]{'-', '-', '-', '-', '-'};
        key = ""+((int)(Math.random()*999));
        point = new Point(-1,-1);
        index=0;
    }

    /**
     * Construct Player with specified ID and default Char Array, Key, Point, and Index
     * @param id id of Player
     */
    public Player(String id){
        this.id = id;
        this.key = ""+((int)(Math.random()*999));
        charArray = new char[]{'-', '-', '-', '-', '-'};
        point = new Point();
        index =0;
    }

    /**
     * Construct Player with specified ID and Key
     * @param id id of Player
     * @param key Key of Player
     */
    public Player(String id, String key){
        this.id = id;
        this.key = key;
        charArray = new char[]{'-', '-', '-', '-', '-'};
        point = new Point();
        index =0;
    }

    /**
     * Return id
     * @return Id
     */
    public String getId(){
        return id;
    }

    /**
     * Return charArray
     * @return charArray
     */
    public char[] getCharArray(){
        return charArray;
    }

    /**
     * Return Key
     * @return Key of Player
     */
    public String getKey(){
        return key;
    }

    /**
     * Set new Key for Player
     * @param key Key of Player
     */
    public void setKey(String key){
        this.key = key;
    }

    /**
     * Adds specified character to char Array of Player
     * @param character character to add to Array
     */
    public void addToCharArray(char character){
        if( character != ' ') {
            charArray[index%5] = character;
            index++;
        }
    }

    /**
     * Sets CharArray to empty default
     */
    public void restList(){
        charArray = new char[]{'-', '-', '-', '-', '-'};;
        index=0;
    }

    /**
     * Set Point of x, y int values of Player to -1, -1
     */
    public void restPoint(){
        point.setLocation(-1,-1);
    }

    /**
     * Return Point
     * @return Point of Player
     */
    public Point getPoint() {
        return point;
    }

    /**
     * Set Point of player to specified Point
     * @param point Point of Player
     */
    public void setPoint(Point point) {
        this.point = point;
    }

    /**
     * Returns a string representation of char Array
     * @return a string representation of char Array
     */
    public String showList(){

        return  Arrays.toString(charArray);
    }

    /**
     * Returns int of hash using Key of Player
     * @return hashed Key
     */
    public int hash() {
        char[] alpha = {'a','b','c','d','e','f','g','h','i','j','k','l',
                'm','n','o','p','q','r','s','t','u','v','w','x','y','z','0','1','2',
                '3','4','5','6','7','8','9'};
        int hash=0;
        for(int i =0; i<key.length(); i++) {
            for(int c=0; c<alpha.length; c++) {
                if(key.charAt(i) == alpha[c]) {
                    hash = hash+((i+1)*(c+1));
                    break;
                }
            }
        }
        return hash;
    }

    /**
     * Returns a string representation of Player
     * @return a string representation of Player data fields
     */
    public String toString(){
        return "ID: " + id +
                " Key: " + key+ " X "+(int)point.getX()+" - Y "+(int)point.getY();
    }

    /**
     * Returns boolean determining if specified object is the same
     * @return boolean
     */
    public boolean equals(Object obj){
        if (obj == this) return true;

        if (obj == null) return false;

        if (this.getClass() == obj.getClass()){
            Player a = (Player) obj;

            return this.getId().equals(a.getId())&&
                    this.getCharArray().equals(a.getCharArray()) &&
                    this.getKey() == a.getKey();
        }
        else
            return false;
    }
}
